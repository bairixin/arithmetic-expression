/**
 * MIT License
 *
 * Copyright (c) 2022 Rixin Bai(白日昕)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdbool.h>
#include "src/stack.h"


#define ARRAY_SIZE(x)   (sizeof(x)/sizeof(x[0]))


static bool is_blank(char c)
{
    char blanks[] = {' ', '\t'};
    size_t i;

    for (i = 0; i < ARRAY_SIZE(blanks); i++)
    {
        if (c == blanks[i])
            return true;
    }
    return false;
}

static int operator_index(char c)
{
    char operators[] = {'+', '-', '*', '/', '(', ')', '\0'};
    size_t i;

    for (i = 0; i < ARRAY_SIZE(operators); i++)
    {
        if (c == operators[i])
            return i;
    }
    return -1;
}

static bool is_operator(char c)
{
    return operator_index(c) >= 0;
}

static int compare_operator(char a, char b)
{
    int map[7][7] =
    {
        //+   -   *   /   (   )   0
        { 1,  1, -1, -1, -1,  1,  1},   // +
        { 1,  1, -1, -1, -1,  1,  1},   // -
        { 1,  1,  1,  1, -1,  1,  1},   // *
        { 1,  1,  1,  1, -1,  1,  1},   // /
        {-1, -1, -1, -1, -1,  0,  2},   // (
        { 1,  1,  1,  1,  2,  1,  1},   // )
        {-1, -1, -1, -1, -1,  2,  0},   // 0
    };
    int index_a;
    int index_b;

    index_a = operator_index(a);
    index_b = operator_index(b);

    return map[index_a][index_b];
}

static int operate(int a, char op, int b)
{
    switch (op)
    {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            return a / b;
        default:
            break;
    }
    return 0;
}


//----------------------------------------------------------------------------------------------------
// operator stack
//----------------------------------------------------------------------------------------------------
static void operator_stack_push(stack_t *s, char operator)
{
    stack_push(s, &operator);
}

static char operator_stack_pop(stack_t *s)
{
    return *((char *)stack_pop(s));
}

static char operator_stack_top(stack_t *s)
{
    return *((char *)stack_top(s));
}


//----------------------------------------------------------------------------------------------------
// operand stack
//----------------------------------------------------------------------------------------------------
static void operand_stack_push(stack_t *s, int operand)
{
    stack_push(s, &operand);
}

static int operand_stack_pop(stack_t *s)
{
    return *((int *)stack_pop(s));
}

static int operand_stack_top(stack_t *s)
{
    return *((int *)stack_top(s));
}


static int arithmetic_expression(const char *exp)
{
    stack_t operand_stack;
    stack_t operator_stack;
    int result = 0;

    init_stack(&operand_stack, sizeof(int));
    init_stack(&operator_stack, sizeof(char));

    operator_stack_push(&operator_stack, '\0');
    while ((*exp != '\0') || (operator_stack_top(&operator_stack) != '\0'))
    {
        if (is_blank(*exp))
        {
            exp++;
        }
        else if (is_operator(*exp))
        {
            switch (compare_operator(operator_stack_top(&operator_stack), *exp))
            {
                case -1:
                    operator_stack_push(&operator_stack, *exp);
                    exp++;
                    break;
                case 0:
                    stack_pop(&operator_stack);
                    exp++;
                    break;
                case 1:
                {
                    char op = operator_stack_pop(&operator_stack);
                    int b = operand_stack_pop(&operand_stack);
                    int a = operand_stack_pop(&operand_stack);
                    int res;

                    res = operate(a, op, b);
                    stack_push(&operand_stack, &res);
                    break;
                }
            default:
                printf("error\n");
                break;
            }
        }
        else
        {
            operand_stack_push(&operand_stack, *exp - '0');
            exp++;
        }
    }

    result = operand_stack_top(&operand_stack);

    clean_stack(&operand_stack);
    clean_stack(&operator_stack);

    return result;
}


int main(int argc, char *argv[])
{
    const char *exp1 = "4 + 2 * ( 3 + 1) - 8 / 4";

    printf("%d\n", arithmetic_expression(exp1));

    return 0;
}
