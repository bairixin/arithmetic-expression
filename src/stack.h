/**
 * MIT License
 *
 * Copyright (c) 2022 Rixin Bai(白日昕)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef STACK_H_
#define STACK_H_

#include <stddef.h>
#include <stdbool.h>


#define INITIAL_SIZE    256
#define INCREASE_SIZE   64


typedef struct stack
{
    void *top;
    void *base;
    size_t item_size;
    size_t allocated;
} stack_t;


bool init_stack(stack_t *s, size_t item_size);
void clean_stack(stack_t *s);

bool stack_push(stack_t *s, void *item);
void* stack_pop(stack_t *s);

void* stack_top(stack_t *s);
bool stack_is_empty(stack_t *s);


#endif // STACK_H_
