/**
 * MIT License
 *
 * Copyright (c) 2022 Rixin Bai(白日昕)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string.h>
#include <stdlib.h>
#include "stack.h"


bool init_stack(stack_t *s, size_t item_size)
{
    s->item_size = item_size;
    s->allocated = s->item_size * INITIAL_SIZE;

    s->top = s->base = malloc(s->allocated);
    if (s->base == NULL)
        return false;

    return true;
}

void clean_stack(stack_t *s)
{
    free(s->base);
    s->allocated = 0;
}

static bool ensure(stack_t *s)
{
    size_t new_size;
    size_t offset;

    if ((size_t)s->top <= ((size_t)s->base + s->allocated - s->item_size))
        return true;

    new_size = s->allocated + INCREASE_SIZE * s->item_size;
    offset = (size_t)s->top - (size_t)s->base;

    s->base = realloc(s->base, new_size);
    if (s->base == NULL)
        return false;

    s->allocated = new_size;
    s->top = (void *)((size_t)s->base + offset);

    return true;
}

bool stack_push(stack_t *s, void *item)
{
    if (!ensure(s))
        return false;

    s->top = (void *)((size_t)s->top + s->item_size);
    memcpy(s->top, item, s->item_size);

    return true;
}

void* stack_pop(stack_t *s)
{
    void *top = s->top;

    if ((size_t)top < (size_t)s->base + s->item_size)
        return NULL;

    s->top = (void *)((size_t)s->top - s->item_size);

    return top;
}


void* stack_top(stack_t *s)
{
    void *top = s->top;

    if ((size_t)top < (size_t)s->base + s->item_size)
        return NULL;

    return top;
}

bool stack_is_empty(stack_t *s)
{
    if (s->allocated <= 0)
        return true;

    return (size_t)s->top > (size_t)s->base ? false : true;
}
